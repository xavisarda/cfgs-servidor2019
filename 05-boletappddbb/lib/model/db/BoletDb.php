<?php 

require_once(__DIR__.'/../Bolet.php');

class BoletDb{
    
  private $_conn;
    
  public function listBolets(){
    //OPEN CONNECTION TO DATABASE
    $this->openConnection();
     
    //PREPARE STATEMENT WITH NO PARAMETERS AT THE MOMENT
    $query = "SELECT * FROM ".DDBB_TABLE;
    $stmt = $this->_conn->prepare($query);
    
    //EXECUTE QUERY
    $stmt->execute();
    $res = $stmt->get_result();
    
    //RETRIEVE RESULTS AND BUILD RETURN ARRAY
    $blts = array();
    while ($blt = $res->fetch_assoc() ) {
      $clrs = explode(",",$blt['colors']);
      array_push($blts, new Bolet($blt['name'],$blt['poison'],$blt['region'],$clrs, $blt['bid']));
    }
    return $blts;
  }
  
  public function getBoletById($i){
    //OPEN CONNECTION TO DATABASE
    $this->openConnection();
     
    //PREPARE STATEMENT WITH ONE PARAMETER
    $query = "SELECT * FROM ".DDBB_TABLE." WHERE bid = ?";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("i", $index);
    $index = $i;
    
    //EXECUTE QUERY
    $stmt->execute();
    $res = $stmt->get_result();
    
    //RETRIEVE RESULT AND BUILD RETURN OBJECT
    $blt = $res->fetch_assoc();
    $clrs = explode(",",$blt['colors']);
    return new Bolet($blt['name'],$blt['poison'],$blt['region'],$clrs,$blt['bid']);
  }
  
  public function updateBolet($n, $p, $r, $c, $i){
    //OPEN CONNECTION TO DATABASE
    $this->openConnection();
     
    //PREPARE STATEMENT WITH SEVERAL PARAMETERS
    $query = "UPDATE ".DDBB_TABLE." SET name = ?, poison = ?, region = ?, colors = ? WHERE bid = ?";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("sissi", $sqln, $sqlp, $sqlr, $sqlc, $sqli);
    $sqln = $n;
    $sqlp = $p;
    $sqlr = $r;
    $sqlc = $c;
    $sqli = $i;
    
    //EXECUTE QUERY
    $stmt->execute();
    
    //RETRIEVE NEW BOLET AND RETURN
    return $this->getBoletById($i);
  }

  
  public function deleteBoletById($i){
    //OPEN CONNECTION TO DATABASE
    $this->openConnection();
    
    //RETRIEVE INFO
    $bolet = $this->getBoletById($i);
    
    //PREPARE STATEMENT WITH ONE PARAMETER
    $query = "DELETE FROM ".DDBB_TABLE." WHERE bid = ?";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("i", $index);
    $index = $i;
    
    //EXECUTE QUERY
    $stmt->execute();
    
    return $bolet;
  }
  
  public function insertBolet($n, $p, $r, $c){
    //OPEN CONNECTION TO DATABASE
    $this->openConnection();
     
    //PREPARE STATEMENT WITH SEVERAL PARAMETERS
    $query = "INSERT INTO ".DDBB_TABLE." (name, poison, region, colors) VALUES (?,?,?,?)";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("siss", $sqln, $sqlp, $sqlr, $sqlc);
    $sqln = $n;
    $sqlp = $p;
    $sqlr = $r;
    $sqlc = $c;
    
    //EXECUTE QUERY
    $stmt->execute();
    
    //RETRIEVE NEW BOLET AND RETURN
    return $this->getBoletById($stmt->insert_id);
  }
  
  private function openConnection(){
    if($this->_conn == NULL){
      $this->_conn = mysqli_connect(DDBB_HOST, DDBB_USER, DDBB_PWD, DDBB_DDBB);    
    }
  }
    
}