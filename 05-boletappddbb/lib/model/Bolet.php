<?php

class Bolet{
    
    private $_name;
    private $_poison;
    private $_region;
    private $_colors;
    private $_bid;
    
    public function __construct($n, $p = true, $region = "", $cs = ["#ff0000", "#6e530b"], $bid = null){
      $this->setName($n);
      $this->setColors($cs);
      $this->setPoison($p);
      $this->setRegion($region);
      $this->_bid = $bid;
    }
    
    public function getName(){
        return $this->_name;
    }
    
    public function getColors(){
        return $this->_colors;
    }
    
    public static function getDefaultColors(){
        return ["#ff0000", "#6e530b"];
    }
    
    public function getPoison(){
        return $this->_poison;
    }
    
    public function getRegion(){
        return $this->_region;
    }
    
    public function getBid(){
        return $this->_bid;
    }
    
    public function setName($n){
        $this->_name = $n;
    }
    
    public function setColors($c){
        $this->_colors = $c;
    }
    
    public function setPoison($p){
        $this->_poison = $p;
    }
    
    public function setRegion($r){
        $this->_region = $r;
    }
    
}