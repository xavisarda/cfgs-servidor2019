<?php

require_once(__DIR__.'/../model/db/BoletDb.php');
require_once(__DIR__.'/../model/Bolet.php');

class BoletCnt{
    
    public function boletList(){
        $db = new BoletDb();
        return $db->listBolets();
    }
    
    public function boletDetails($id){
        $db = new BoletDb();
        return $db->getBoletById($id);
    }
    
    public function removeBoletByIndex($id){
        $db = new BoletDb();
        return $db->deleteBoletById($id);
    }
    
    public function addBolet($xn, $xp, $xr, $xcs){
        $cntn = $xn;
        $cntp = $xp == 'yes' ? true : false;
        $cntr = $xr;
        $cntcs = count($xcs) != 0 ? implode(",", $xcs) : implode(",", Bolet::getDefaultColors());
        
        $db = new BoletDb();
        return $db->insertBolet($cntn, $cntp, $cntr, $cntcs);
    }
    
    public function updateBolet($xn, $xp, $xr, $xcs, $xin){
        $cntn = $xn;
        $cntp = $xp == 'yes' ? true : false;
        $cntr = $xr;
        $cntcs = count($xcs) != 0 ? implode(",", $xcs) : implode(",", Bolet::getDefaultColors());
        $cnti = $xin;
        
        $db = new BoletDb();
        return $db->updateBolet($cntn, $cntp, $cntr, $cntcs, $cnti);
    }
}