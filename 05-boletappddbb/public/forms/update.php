<?php 

require_once(__DIR__.'/../../lib/inc/constants.php');
require_once(__DIR__.'/../../lib/controller/BoletCnt.php');

session_start();

$name = $_POST['bn'];
$colors = array();
if(isset($_POST['bcs']) && $_POST['bcs'] != null && is_array($_POST['bcs'])){
    $colors = $_POST['bcs'];
}
$reg = $_POST['breg'];
$pois = $_POST['bpois'];
$index = $_POST['blti'];


$cnt = new BoletCnt();
$dr = $cnt->updateBolet($name, $pois, $reg, $colors, $index);

$title_pag = "Bolet successfully updated";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1><?=$title_pag?></h1>
      <dl>
        <dt>Name</dt>
        <dd><?=$dr->getName()?>
        <dt>Is posionous</dt>
        <dd><?=$dr->getPoison() == 0 ? "No" : "Yes" ?>
        <dt>Region</dt>
        <dd><?=$dr->getRegion()?>
        <dt>Colors</dt>
<?php foreach($dr->getColors() as $clr) { ?>
        <dd><?=$clr?>
<?php } ?>
      </dl>
      <a href="/">Back to index</a>
      <?php include('../footer.php');?>
    </div>
  </body>
</html>