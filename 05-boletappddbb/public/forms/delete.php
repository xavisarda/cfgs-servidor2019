<?php 

require_once(__DIR__.'/../../lib/inc/constants.php');
require_once(__DIR__.'/../../lib/controller/BoletCnt.php');

session_start();

$ndx = $_GET['index'];

$cnt = new BoletCnt();
$dr = $cnt->removeBoletByIndex($ndx);

$title_pag = "Bolet deleted";

?><html>
<?php include_once(__DIR__.'/../../lib/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1><?=$title_pag?></h1>
      <span>This bolet has been deleted</span>
      <dl>
        <dt>Name</dt>
        <dd><?=$dr->getName()?>
        <dt>Is posionous</dt>
        <dd><?=$dr->getPoison() == 0 ? "No" : "Yes" ?>
        <dt>Region</dt>
        <dd><?=$dr->getRegion()?>
        <dt>Colors</dt>
<?php foreach($dr->getColors() as $clr) { ?>
        <dd><?=$clr?>
<?php } ?>
      </dl>
      <a href="/">Back to index</a>
    </div>
  </body>
</html>