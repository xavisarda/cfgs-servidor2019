<?php 

$title_pag = "Bolet add";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1><?=$title_pag?></h1>
      <form action="/forms/add.php" method="post">
      <dl>
        <dt>Name</dt>
        <dd><input type="text" name="bn"/></dd>
        <dt>Colors</dt>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-w" value="#ffffff"/><label for="bcs-w">white</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-b" value="#000000"/><label for="bcs-b">black</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-r" value="#ff0000"/><label for="bcs-r">red</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-bl" value="#0000ff"/><label for="bcs-bl">blue</label>
        </dd>
        <dt>Poisonous</dt>
        <dd>
          <input type="radio" name="bpois" id="p-y" value="yes" checked/>
          <label for="p-y">Yes</label>
        </dd>
        <dd>
          <input type="radio" name="bpois" id="p-n" value="no"/>
          <label for="p-n">No</label>
        </dd>
        <dt>Region</dt>
        <dd>
          <select name="breg">
            <option value="Alt Camp">Alt Camp</option><option value="Alt Empordà">Alt Empordà</option>
            <option value="Alt Penedès">Alt Penedès</option><option value="Alt Urgell">Alt Urgell</option>
            <option value="Alta Ribagorça">Alta Ribagorça</option><option value="Anoia">Anoia</option>
            <option value="Aran">Aran</option><option value="Bages">Bages</option>
            <option value="Baix Camp">Baix Camp</option><option value="Baix Ebre">Baix Ebre</option>
            <option value="Baix Empordà">Baix Empordà</option><option value="Baix Llobregat">Baix Llobregat</option>
            <option value="Baix Penedès">Baix Penedès</option><option value="Barcelonès">Barcelonès</option>
            <option value="Berguedà" selected>Berguedà</option><option value="Cerdanya">Cerdanya</option>
            <option value="Conca de Barberà">Conca de Barberà</option><option value="Garraf">Garraf</option>
            <option value="Garrigues">Garrigues</option><option value="Garrotxa">Garrotxa</option>
            <option value="Gironès">Gironès</option><option value="Maresme">Maresme</option>
            <option value="Moianès">Moianès</option><option value="Montsià">Montsià</option>
            <option value="Noguera">Noguera</option><option value="Osona">Osona</option>
            <option value="Pallars Jussà">Alt Camp</option><option value="Pallars Sobirà">Pallars Sobirà</option>
            <option value="Pla de l'Estany">Alt Camp</option><option value="Pla d'Urgell">Pla d'Urgell</option>
            <option value="Priorat">Priorat</option><option value="Ribera d'Ebre">Ribera d'Ebre</option>
            <option value="Ripollès">Ripollès</option><option value="Segarra">Segarra</option>
            <option value="Segrià">Segrià</option><option value="Selva">Selva</option>
            <option value="Solsonès">Solsonès</option><option value="Tarragonès">Tarragonès</option>
            <option value="Terra Alta">Terra Alta</option><option value="Urgell">Urgell</option>
            <option value="Vallès Occidental">Vallès Occidental</option><option value="Vallès Oriental">Vallès Oriental</option>
          </select>
        </dd>
        <dd><input type="submit" name="bs" value="Crear"/></dd>
      </dl>
      </form>
      <a href="/">Back home</a>
    </div>
  </body>
</html>