<?php 

require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/BoletCnt.php');

session_start();

$ndx = $_GET['index'];

$cnt = new BoletCnt();
$dr = $cnt->boletDetails($ndx);

$title_pag = "Bolet update";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1><?=$title_pag?></h1>
      <form action="/forms/update.php" method="post">
      <dl>
        <dt>Name</dt>
        <dd><input type="text" name="bn" value="<?=$dr->getName()?>"/></dd>
        <dt>Colors</dt>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-w" value="#ffffff" <?php if(in_array("#ffffff",$dr->getColors())){ echo "checked"; } ?>/>
          <label for="bcs-w">white</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-b" value="#000000" <?php if(in_array("#000000",$dr->getColors())){ echo "checked"; } ?>/>
          <label for="bcs-b">black</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-r" value="#ff0000" <?php if(in_array("#ff0000",$dr->getColors())){ echo "checked"; } ?>/>
          <label for="bcs-r">red</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-bl" value="#0000ff" <?php if(in_array("#0000ff",$dr->getColors())){ echo "checked"; } ?>/>
          <label for="bcs-bl">blue</label>
        </dd>
        <dt>Poisonous</dt>
        <dd>
          <input type="radio" name="bpois" id="p-y" value="yes" <?php if($dr->getPoison()){ echo "checked"; } ?>/>
          <label for="p-y">Yes</label>
        </dd>
        <dd>
          <input type="radio" name="bpois" id="p-n" value="no" <?php if(!$dr->getPoison()){ echo "checked"; } ?>/>
          <label for="p-n">No</label>
        </dd>
        <dt>Region</dt>
        <dd>
          <select name="breg">
            <option value="Alt Camp" <?php if($dr->getRegion()=="Alt Camp"){ echo "selected"; } ?>>Alt Camp</option>
            <option value="Alt Empordà" <?php if($dr->getRegion()=="Alt Empordà"){ echo "selected"; } ?>>Alt Empordà</option>
            <option value="Alt Penedès" <?php if($dr->getRegion()=="Alt Penedès"){ echo "selected"; } ?>>Alt Penedès</option>
            <option value="Alt Urgell" <?php if($dr->getRegion()=="Alt Urgell"){ echo "selected"; } ?>>Alt Urgell</option>
            <option value="Alta Ribagorça" <?php if($dr->getRegion()=="Alta Ribagorça"){ echo "selected"; } ?>>Alta Ribagorça</option>
            <option value="Anoia" <?php if($dr->getRegion()=="Anoia"){ echo "selected"; } ?>>Anoia</option>
            <option value="Aran" <?php if($dr->getRegion()=="Aran"){ echo "selected"; } ?>>Aran</option>
            <option value="Bages" <?php if($dr->getRegion()=="Bages"){ echo "selected"; } ?>>Bages</option>
            <option value="Baix Camp" <?php if($dr->getRegion()=="Baix Camp"){ echo "selected"; } ?>>Baix Camp</option>
            <option value="Baix Ebre" <?php if($dr->getRegion()=="Baix Ebre"){ echo "selected"; } ?>>Baix Ebre</option>
            <option value="Baix Empordà" <?php if($dr->getRegion()=="Baix Empordà"){ echo "selected"; } ?>>Baix Empordà</option>
            <option value="Baix Llobregat" <?php if($dr->getRegion()=="Baix Llobregat"){ echo "selected"; } ?>>Baix Llobregat</option>
            <option value="Baix Penedès" <?php if($dr->getRegion()=="Baix Penedès"){ echo "selected"; } ?>>Baix Penedès</option>
            <option value="Barcelonès" <?php if($dr->getRegion()=="Barcelonès"){ echo "selected"; } ?>>Barcelonès</option>
            <option value="Berguedà" <?php if($dr->getRegion()=="Berguedà"){ echo "selected"; } ?>>Berguedà</option>
            <option value="Cerdanya" <?php if($dr->getRegion()=="Cerdanya"){ echo "selected"; } ?>>Cerdanya</option>
            <option value="Conca de Barberà" <?php if($dr->getRegion()=="Conca de Barberà"){ echo "selected"; } ?>>Conca de Barberà</option>
            <option value="Garraf" <?php if($dr->getRegion()=="Garraf"){ echo "selected"; } ?>>Garraf</option>
            <option value="Garrigues" <?php if($dr->getRegion()=="Garrigues"){ echo "selected"; } ?>>Garrigues</option>
            <option value="Garrotxa" <?php if($dr->getRegion()=="Garrotxa"){ echo "selected"; } ?>>Garrotxa</option>
            <option value="Gironès" <?php if($dr->getRegion()=="Gironès"){ echo "selected"; } ?>>Gironès</option>
            <option value="Maresme" <?php if($dr->getRegion()=="Maresme"){ echo "selected"; } ?>>Maresme</option>
            <option value="Moianès" <?php if($dr->getRegion()=="Moianès"){ echo "selected"; } ?>>Moianès</option>
            <option value="Montsià" <?php if($dr->getRegion()=="Montsià"){ echo "selected"; } ?>>Montsià</option>
            <option value="Noguera" <?php if($dr->getRegion()=="Noguera"){ echo "selected"; } ?>>Noguera</option>
            <option value="Osona" <?php if($dr->getRegion()=="Osona"){ echo "selected"; } ?>>Osona</option>
            <option value="Pallars Jussà" <?php if($dr->getRegion()=="Pallars Jussà"){ echo "selected"; } ?>>Alt Camp</option>
            <option value="Pallars Sobirà" <?php if($dr->getRegion()=="Pallars Sobirà"){ echo "selected"; } ?>>Pallars Sobirà</option>
            <option value="Pla de l'Estany" <?php if($dr->getRegion()=="Pla de l'Estany"){ echo "selected"; } ?>>Alt Camp</option>
            <option value="Pla d'Urgell" <?php if($dr->getRegion()=="Pla d'Urgell"){ echo "selected"; } ?>>Pla d'Urgell</option>
            <option value="Priorat" <?php if($dr->getRegion()=="Priorat"){ echo "selected"; } ?>>Priorat</option>
            <option value="Ribera d'Ebre" <?php if($dr->getRegion()=="Ribera d'Ebre"){ echo "selected"; } ?>>Ribera d'Ebre</option>
            <option value="Ripollès" <?php if($dr->getRegion()=="Ripollès"){ echo "selected"; } ?>>Ripollès</option>
            <option value="Segarra" <?php if($dr->getRegion()=="Segarra"){ echo "selected"; } ?>>Segarra</option>
            <option value="Segrià" <?php if($dr->getRegion()=="Segrià"){ echo "selected"; } ?>>Segrià</option>
            <option value="Selva" <?php if($dr->getRegion()=="Selva"){ echo "selected"; } ?>>Selva</option>
            <option value="Solsonès" <?php if($dr->getRegion()=="Solsonès"){ echo "selected"; } ?>>Solsonès</option>
            <option value="Tarragonès" <?php if($dr->getRegion()=="Tarragonès"){ echo "selected"; } ?>>Tarragonès</option>
            <option value="Terra Alta" <?php if($dr->getRegion()=="Terra Alta"){ echo "selected"; } ?>>Terra Alta</option>
            <option value="Urgell" <?php if($dr->getRegion()=="Urgell"){ echo "selected"; } ?>>Urgell</option>
            <option value="Vallès Occidental" <?php if($dr->getRegion()=="Vallès Occidental"){ echo "selected"; } ?>>Vallès Occidental</option>
            <option value="Vallès Oriental" <?php if($dr->getRegion()=="Vallès Oriental"){ echo "selected"; } ?>>Vallès Oriental</option>
          </select>
        </dd>
        <dd>
          <input type="hidden" name="blti" value="<?=$ndx?>"/>
          <input type="submit" name="bs" value="Update"/>
        </dd>
      </dl>
      </form>
      <a href="/">Back home</a>
    </div>
  </body>
</html>