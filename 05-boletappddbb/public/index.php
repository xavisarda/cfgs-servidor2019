<?php 

require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/BoletCnt.php');

session_start();

$cnt = new BoletCnt();
$drs = $cnt->boletList();

$title_pag = "Bolet list";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <a href="/add.php">Add bolet</a>
      <h1><?=$title_pag?></h1>
      <table>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>colors</th>
          <th>&nbsp;</th>
          <th>&nbsp;</th>
        </tr>
<?php foreach($drs as $dr){ ?>
        <tr>
          <td><?=$dr->getBid()?></td>
          <td><a href="/details.php?index=<?=$dr->getBid()?>"><?=$dr->getName()?></a></td>
          <td><?php $dr->getColors()?></td>
          <td><a href="/update.php?index=<?=$dr->getBid()?>">Update</a></td>
          <td><a href="/forms/delete.php?index=<?=$dr->getBid()?>">Delete</a></td>
        </tr>
<?php } ?>
      </table>
      <?php include('footer.php');?>
      
      <iframe width="560" height="315" src="https://www.youtube.com/embed/8imDPFtw2P4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </body>
</html>