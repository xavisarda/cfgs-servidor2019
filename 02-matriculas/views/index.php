<?php 

require_once(__DIR__.'/../lib/controller/LicensePlateController.php');

$cnt = new LicensePlateController();
$licenses = $cnt->getLicensePlates();

$pag_title = "License plate list"
?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <h1><?=$pag_title?></h1>
    <table>
        <tr><th>Licese num.</th><th>color</th></tr>
<?php foreach ($licenses as $i => $l) { ?>
        <tr>
          <td><a href="/details.php?in=<?=$i?>"><?=$l->getLicnum()?></a></td>
          <td><?=$l->getColor()?></td>
        </tr>
<?php } ?>
    </table>
    <hr/>
  </body>
</html>