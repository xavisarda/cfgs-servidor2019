<?php 

require_once(__DIR__.'/../lib/controller/LicensePlateController.php');

$index = $_GET['in'];

$cnt = new LicensePlateController();
$license = $cnt->getLicensePlateByIndex($index);

$pag_title = "License plate detail"
?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <h1><?=$pag_title?></h1>
    <h2><?=$license->getLicnum()?> details</h2>
    <dl>
        <dt><?=$license->getLicnum()?></dt>
        <dd><?=$license->getColor()?></dd>
    </dl>
    <a href="/">back to index</a>
  </body>
</html>