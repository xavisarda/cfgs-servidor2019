<?php

require_once(__DIR__.'/../model/LicensePlate.php');

class LicensePlateController{
    
    private $_charsinc = [ "B","C","D","F","G","H","J","K","L","M",
            "N","P","Q","R","S","T","V","W","X","Y","Z"];
    private $_colors = [ "red", "blue", "white", "black"];
    
    public function getLicensePlates($numw = 3){
        return $this->generateLic($numw);
    }
    
    public function getLicensePlateByIndex($index, $numw = 3){
        $licenses = $this->generateLic($numw);
        return $licenses[$index];
    }
    
    private function generateLic($numw){
        $charslic = $this->generateCharLic($numw);
        $licenses = array();
        foreach($charslic as $cl){
            for( $i = 0 ; $i <= 9999 ; $i++){
                if($i < 10){
                    array_push($licenses, new LicensePlate('000'.$i.' '.$cl, $this->_colors[rand(0,3)])); 
                } elseif($i >= 10 && $i < 100){
                    array_push($licenses, new LicensePlate('00'.$i.' '.$cl, $this->_colors[rand(0,3)])); 
                } elseif($i >= 100 && $i < 1000){
                    array_push($licenses, new LicensePlate('0'.$i.' '.$cl, $this->_colors[rand(0,3)])); 
                } else{
                    array_push($licenses, new LicensePlate($i.' '.$cl, $this->_colors[rand(0,3)])); }
            }
        }
        return $licenses;
    }
    
    private function generateCharLic($num){
        $arraylicw = array();
        for($ci1 = 0 ; $ci1 < count($this->_charsinc) ; $ci1++){
            for($ci2 = 0 ; $ci2 < count($this->_charsinc) ; $ci2++){
                for($ci3 = 0 ; $ci3 < count($this->_charsinc) ; $ci3++){
                    $licw = $this->_charsinc[$ci1].$this->_charsinc[$ci2].$this->_charsinc[$ci3];
                    array_push($arraylicw, $licw);
                    if(count($arraylicw) >= $num){
                        return $arraylicw;    
                    }
                }
            }
        }
    }
}