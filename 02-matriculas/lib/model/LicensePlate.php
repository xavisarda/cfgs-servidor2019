<?php 

class LicensePlate{
    
    private $_licnum;
    private $_color;
    
    public function __construct($licn = "", $col = ""){
        $this->setColor($col);
        $this->setLicnum($licn);
    }
    
    public function getLicnum(){
        return $this->_licnum;    
    }
    
    public function getColor(){
        return $this->_color;    
    }
    
    public function setLicnum($ln){
        $this->_licnum = $ln;    
    }
    
    public function setColor($c){
        $this->_color = $c;
    }
    
}