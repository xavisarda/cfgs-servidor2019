<?php

class Drink{
    
    private $_name;
    private $_year;
    private $_alcohol;
    
    public function __construct($n = "", $y = 0, $a = 0){
        $this->setName($n);
        $this->setYear($y);
        $this->setAlcohol($a);
    }
    
    public function getName(){
        return $this->_name;
    }
    
    public function getYear(){
        return $this->_year;
    }
    
    public function getAlcohol(){
        return $this->_alcohol;
    }
    
    public function setName($n){
        $this->_name = $n;
    }
    
    public function setYear($y){
        $this->_year = $y;
    }
    
    public function setAlcohol($a){
        $this->_alcohol = $a;
    }
    
}