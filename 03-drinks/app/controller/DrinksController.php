<?php

require_once(__DIR__.'/../model/Drink.php');


class DrinksController{
    
    private $data = [ ["name" => "Glenrothes", "year" => 1999, "alc" => 34],
                ["name" => "Jura Origin", "year" => 2001, "alc" => 38],
                ["name" => "Glennfiddich", "year" => 2000, "alc" => 42],
    ];
    
    public function drinksList(){
        if(isset($_SESSION['drinkslist']) 
                && $_SESSION['drinkslist'] != null 
                && is_array($_SESSION['drinkslist'])){
            return $_SESSION['drinkslist'];
        }
        
        $drinks = array();
        
        foreach($this->data as $d){
            $drinks[] = new Drink($d['name'], $d['year'], $d['alc']);
        }
        
        $_SESSION['drinkslist'] = $drinks;
        return $drinks;
    }
    
    public function drinkByIndex($i){
        $drinks = $this->drinksList();
        return $drinks[$i];
    }
    
    public function addNewDrink($n, $y = 0, $a = 0){
        $ndrink = new Drink($n, $y, $a);
        array_push($_SESSION['drinkslist'], $ndrink);
        return $ndrink;
    }
    
}