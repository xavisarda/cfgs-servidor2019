<?php 

require_once(__DIR__.'/../../app/controller/DrinksController.php');

session_start();

$name = $_POST['dn'];
$year = $_POST['dy'];
$alco = $_POST['da'];

$cnt = new DrinksController();
$dr = $cnt->addNewDrink($name, $year, $alco);

$title_pag = "New drink added";

?><html>
<?php include_once(__DIR__.'/../app/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1><?=$title_pag?></h1>
      <dl>
        <dt><?=$dr->getName()?></dt>
        <dd><?=$dr->getYear()?></dd>
        <dd><?=$dr->getAlcohol()?></dd>
      </dl>
      <a href="/">Back home</a>
    </div>
  </body>
</html>