<?php 

require_once(__DIR__.'/../app/controller/DrinksController.php');

session_start();

$cnt = new DrinksController();
$drs = $cnt->drinksList();

$title_pag = "Drink list";

?><html>
<?php include_once(__DIR__.'/../app/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <a href="/add.php">Add drink</a>
      <h1><?=$title_pag?></h1>
      <table>
        <tr><th>Name</th><th>Year</th></tr>
<?php foreach($drs as $i => $dr){ ?>
        <tr>
          <td><a href="/details.php?index=<?=$i?>"><?=$dr->getName()?></a></td>
          <td><?=$dr->getYear()?></td>
        </tr>
<?php } ?>
      </table>
    </div>
  </body>
</html>