<?php 

require_once(__DIR__.'/../app/controller/DrinksController.php');

session_start();

$in = $_GET['index'];

$cnt = new DrinksController();
$dr = $cnt->drinkByIndex($in);

$title_pag = "Drink details";

?><html>
<?php include_once(__DIR__.'/../app/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1><?=$title_pag?></h1>
      <dl>
        <dt><?=$dr->getName()?></dt>
        <dd><?=$dr->getYear()?></dd>
        <dd><?=$dr->getAlcohol()?></dd>
      </dl>
      <a href="/">Back home</a>
    </div>
  </body>
</html>