<?php 

require_once(__DIR__.'/../lib/controller/BoletCnt.php');

session_start();

$cnt = new BoletCnt();
$drs = $cnt->boletList();

$title_pag = "Bolet list";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <a href="/add.php">Add bolet</a>
      <h1><?=$title_pag?></h1>
      <table>
        <tr><th>Name</th><th>colors</th></tr>
<?php foreach($drs as $i => $dr){ ?>
        <tr>
          <td><a href="/details.php?index=<?=$i?>"><?=$dr->getName()?></a></td>
          <td><?php //$dr->getColors()?></td>
        </tr>
<?php } ?>
      </table>
    </div>
  </body>
</html>