<?php 

$title_pag = "Bolet add";

?><html>
<?php include_once(__DIR__.'/../app/inc/head.php'); ?>
  <body>
    <div id="wrapper">
      <h1><?=$title_pag?></h1>
      <form action="/forms/add.php" method="post">
      <dl>
        <dt>Name</dt>
        <dd><input type="text" name="bn"/></dd>
        <dt>Colors</dt>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-w" value="blanc"/>
          <label for="bcs-w">white</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-b" value="negre"/>
          <label for="bcs-b">black</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-r" value="vermell"/>
          <label for="bcs-r">red</label>
        </dd>
        <dd>
          <input type="checkbox" name="bcs[]" id="bcs-bl" value="blau"/>
          <label for="bcs-bl">blue</label>
        </dd>
        <dt>Poisonous</dt>
        <dd>
          <input type="radio" name="bpois" id="p-y" value="yes" checked/>
          <label for="p-y">Yes</label>
        </dd>
        <dd>
          <input type="radio" name="bpois" id="p-n" value="no"/>
          <label for="p-n">No</label>
        </dd>
        <dd><input type="submit" name="bs" value="Crear"/></dd>
      </dl>
      </form>
      <a href="/">Back home</a>
    </div>
  </body>
</html>