<?php

class Bolet{
    
    private $_name;
    private $_colors;
    
    public function __construct($n, $cs = ["#ff0000", "#6e530b"]){
      $this->setName($n);
      $this->setColors($cs);
    }
    
    public function getName(){
        return $this->_name;
    }
    
    public function getColors(){
        return $this->_colors;
    }
    
    public function setName($n){
        $this->_name = $n;
    }
    
    public function setColors($c){
        $this->_colors = $c;
    }
    
    
}