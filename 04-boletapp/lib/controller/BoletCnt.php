<?php

require_once(__DIR__.'/../model/Bolet.php');

class BoletCnt{
    
    public function boletList(){
        return $_SESSION['bList'];
    }
    
    public function boletDetails($id){
        
    }
    
    public function addBolet($xn, $xcs){
        if(count($xcs) == 0){
            $xbolet = new Bolet($xn);    
        }else{
            $xbolet = new Bolet($xn, $xcs);    
        }
        if(!is_array($_SESSION['bList'])){
            $_SESSION['bList'] = array();
        }
        array_push($_SESSION['bList'], $xbolet);
        return $xbolet;
    }
}