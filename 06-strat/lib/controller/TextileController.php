<?php 

require_once(__DIR__.'/../model/TrouserWear.php');
require_once(__DIR__.'/../model/SkirtWear.php');

class TextileController{
    
    public function getList(){
        $tro = new TrouserWear('fall', 'cotton', '60', 
            'black', 'Fine trouser', '32', '34', '35', '40');
        $skrt = new SkirtWear('spring', 'linen', '60',
            'black', 'Fine skirt', '36', '20', 'side', 'plain');
        
        $ret = [$tro, $skrt];
        return $ret;
    }
    
}