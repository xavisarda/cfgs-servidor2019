<?php 

require_once(__DIR__.'/UpperWear.php');

class ShirtWear extends UpperWear{
    
    private $_fit;
    private $_neck;
    
    public function __construct($cws, $cwm, $cwp, $cwc, $cwd, 
        $f, $n){
        $this->setSeason($cws);
        $this->setMaterial($cwm);
        $this->setPrice($cwp);
        $this->setColor($cwc);
        $this->setDesc($cwd);
        
        $this->setFit($f);
        $this->setNeck($n);
    }
    
    public function getFit(){
        return $this->_fit;
    }

    public function getNeck(){
        return $this->_neck;
    }

    public function setFit($_fit){
        $this->_fit = $_fit;
    }

    public function setNeck($_neck){
        $this->_neck = $_neck;
    }

}