<?php

require_once(__DIR__.'/CommonWear.php');

abstract class UpperWear extends CommonWear{
    
    private static $permitted_sizes = [ "XS", "S", "M", "L", "XL", "2XL", "XXL" ];
    
    protected $_size;
    protected $_sleeves;
    
    public function getSize(){
        if(in_array($this->_size, self::$permitted_sizes)){
            return $this->_size;    
        }
        error_log("SizeError: Wrong size defined in UpperWear instance");
        return "ERROR, WRONG SIZE DEFINED";
    }
    
    public function getSleeves(){
        return $this->_sleeves;
    }
    
    public function setSize($v){
        if(in_array($v, self::$permitted_sizes)){
            $this->_size = $v;
        }
        error_log("SizeError: Tried to define wrong size in UpperWear instance");
        $this->_size = "E.";
    }
    
    public function setSleeves($v){
        $this->_sleeves = $v;
    }
    
}