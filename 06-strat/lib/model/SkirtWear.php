<?php

require_once(__DIR__.'/LowerWear.php');

class SkirtWear extends LowerWear{
    
    private $_cut;
    private $_type;
    
    public function __construct($cws, $cwm, $cwp, $cwc, $cwd, 
        $w, $l, $c, $t){
        $this->setSeason($cws);
        $this->setMaterial($cwm);
        $this->setPrice($cwp);
        $this->setColor($cwc);
        $this->setDesc($cwd);
        
        $this->setWaist($w);
        $this->setLength($l);
        $this->setCut($c);
        $this->setType($t);
    }
    
    public function getCut(){
        return $this->_cut;
    }

    public function getType(){
        return $this->_type;
    }

    public function setCut($_cut){
        $this->_cut = $_cut;
    }

    public function setType($_type){
        $this->_type = $_type;
    }
    
    public function toJSON(){
        $data = [ "season" => $this->getSeason(),
            "material" => $this->getMaterial(),
            "price" => $this->getPrice(),
            "color" => $this->getColor(),
            "desc" => $this->getDesc(),
            
            "measurements" => ["waist" => $this->getWaist(),
                "length" => $this->getLength()],
            "cut" => $this->getCut(),
            "type" => $this->getType()];
        
        return json_encode($data);
    }
    
    public function toHTML(){
        $output = "<article>";
        $output.= "  <h4>".$this->getDesc()."</h4>";
        $output.= "  <ul>";
        $output.= "    <li>Season - ".$this->getSeason()."</li>";
        $output.= "    <li>Material - ".$this->getMaterial()."</li>";
        $output.= "    <li>Price - ".$this->getPrice()."</li>";
        $output.= "    <li>Color - ".$this->getColor()."</li>";
        $output.= "    <li>Cut - ".$this->getCut()."</li>";
        $output.= "    <li>Type - ".$this->getType()."</li>";
        $output.= "  </ul>";
        $output.= "  <table>";
        $output.= "    <tr><th>Waist</th><th>Length</th></tr>";
        $output.= "    <tr><td>".$this->getWaist()."</td><td>".$this->getLength()."</td></tr>";
        $output.= "  </table>";
        $output.= "</article>";
        
        return $output;
    }
    
    
}