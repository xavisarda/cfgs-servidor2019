<?php 

require_once(__DIR__.'/UpperWear.php');

class TshirtWear extends UpperWear{
    
    public function __construct($cws, $cwm, $cwp, $cwc, $cwd){
        $this->setSeason($cws);
        $this->setMaterial($cwm);
        $this->setPrice($cwp);
        $this->setColor($cwc);
        $this->setDesc($cwd);
    }
    
}