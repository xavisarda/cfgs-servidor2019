<?php 

abstract class CommonWear{
    
    protected $_season;
    protected $_material;
    protected $_price;
    protected $_color;
    protected $_desc;
    
    public abstract function toHTML();
    public abstract function toJSON();
    
    public function getSeason(){
        return $this->_season;
    }
    
    public function getMaterial(){
        return $this->_material;
    }
    
    public function getPrice(){
        return $this->_price;
    }
    
    public function getColor(){
        return $this->_color;
    }
    
    public function getDesc(){
        return $this->_desc;
    }
    
    public function setSeason($v){
        $this->_season = $v;
    }
    
    public function setMaterial($v){
        $this->_material = $v;
    }
    
    public function setPrice($v){
        $this->_price = $v;
    }
    
    public function setColor($v){
        $this->_color = $v;
    }
    
    public function setDesc($v){
        $this->_desc = $v;
    }
    
}