<?php

require_once(__DIR__.'/LowerWear.php');

class TrouserWear extends LowerWear{
    
    private $_tiro;
    private $_hips;
    
    public function __construct($cws, $cwm, $cwp, $cwc, $cwd, 
        $w, $l, $t, $h){
        $this->setSeason($cws);
        $this->setMaterial($cwm);
        $this->setPrice($cwp);
        $this->setColor($cwc);
        $this->setDesc($cwd);
        
        $this->setWaist($w);
        $this->setLength($l);
        $this->setTiro($t);
        $this->setHips($h);
    }
    
    public function getTiro(){
        return $this->_tiro;
    }

    public function getHips(){
        return $this->_hips;
    }

    public function setTiro($_tiro){
        $this->_tiro = $_tiro;
    }

    public function setHips($_hips){
        $this->_hips = $_hips;
    }
    
    public function toJSON(){
        $data = [ "season" => $this->getSeason(), 
            "material" => $this->getMaterial(),
            "price" => $this->getPrice(),
            "color" => $this->getColor(),
            "desc" => $this->getDesc(),
            
            "measurements" => ["waist" => $this->getWaist(),
            "length" => $this->getLength(),
            "tiro" => $this->getTiro(),
            "hips" => $this->getHips()]];
        
        return json_encode($data);
    }

    public function toHTML(){
        $output = "<article>";
        $output.= "  <h4>".$this->getDesc()."</h4>";
        $output.= "  <ul>";
        $output.= "    <li>Season - ".$this->getSeason()."</li>";
        $output.= "    <li>Material - ".$this->getMaterial()."</li>";
        $output.= "    <li>Price - ".$this->getPrice()."</li>";
        $output.= "    <li>Color - ".$this->getColor()."</li>";
        $output.= "  </ul>";
        $output.= "  <table>";
        $output.= "    <tr><th>Waist</th><th>Length</th><th>Tiro</th><th>Hips</th></tr>";
        $output.= "    <tr><td>".$this->getWaist()."</td><td>".$this->getLength()."</td><td>".$this->getTiro()."</td><td>".$this->getHips()."</td></tr>";
        $output.= "  </table>";
        $output.= "</article>";
        
        return $output;
    }
    
}