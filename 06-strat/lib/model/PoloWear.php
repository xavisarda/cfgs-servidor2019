<?php 

require_once(__DIR__.'/TshirtWear.php');

class PoloWear extends TshirtWear{
    
    private $_neck;
    private $_cuffs;
    
    public function __construct($cws, $cwm, $cwp, $cwc, $cwd,
        $n, $c){
        $this->setSeason($cws);
        $this->setMaterial($cwm);
        $this->setPrice($cwp);
        $this->setColor($cwc);
        $this->setDesc($cwd);
        
        $this->setNeck($n);
        $this->setCuffs($c);
    }
    
    
    public function getNeck(){
        return $this->_neck;
    }

    public function getCuffs(){
        return $this->_cuffs;
    }

    public function setNeck($_neck){
        $this->_neck = $_neck;
    }

    public function setCuffs($_cuffs){
        $this->_cuffs = $_cuffs;
    }
    
}