<?php

require_once(__DIR__.'/CommonWear.php');

abstract class LowerWear extends CommonWear{
    
    protected $_waist;
    protected $_length;
    
    public function getWaist(){
        return $this->_waist;
    }

    public function getLength(){
        return $this->_length;
    }

    public function setWaist($_waist){
        $this->_waist = $_waist;
    }

    public function setLength($_length){
        $this->_length = $_length;
    }
    
}