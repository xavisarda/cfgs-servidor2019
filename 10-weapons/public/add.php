<?php

require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/WeaponController.php');
require_once(__DIR__.'/../lib/model/Weapon.php');
require_once(__DIR__.'/../lib/model/WeaponImg.php');
require_once(__DIR__.'/../lib/model/WeaponYT.php');
require_once(__DIR__.'/../lib/model/WeaponSC.php');

//AQUI LLAMARIAMOS A LA SESSION

$cnt = new WeaponController();
$ty = $_GET['type'];

?><html>
  <head>
    <title>Add Weapon</title>
    <link href="/css/styles.css" type="text/css" rel="stylesheet"/>
  </head>
  <body>
    <div id="wrapper">
      <header>
        <ul>
          <li>Home</li>
          <li>Other pages...</li>
        </ul>
      </header>
      <div id="content">
        <h1>Add Weapon</h1>
        <div id="submenu">
        	<ul>
        		<li><a href="/add.php?type=<?=W_TYPE_NULL?>">Añadir arma</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_IMG?>">Añadir arma imagen</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_YT?>">Añadir arma YT</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_SC?>">Añadir arma SC</a></li>
        	</ul>
        </div>
        <div id="w-container">
        <form method="post" action="/forms/add.php">
        	<dl>
        	  <dt>Nombre</dt><dd><input type="text" name="wname"></dd>
        	  <dt>Origen</dt><dd><input type="text" name="worig"></dd>
        	  <dt>Material</dt><dd><input type="text" name="wmat"></dd>
        	  <dt>Filo</dt><dd><input type="number" name="wfil"></dd>
        	  <dt>Peso</dt><dd><input type="number" name="wpes"></dd>
        	  <?php
        	  $field ="";
        	  switch ($ty){
        	      case W_TYPE_IMG:
        	          $field ="Imagen";
        	          break;
        	      case W_TYPE_YT:
        	          $field ="ID YouTube";
        	          break;
        	      case W_TYPE_SC:
        	          $field ="ID SoundCloud";
        	          break;
        	      case W_TYPE_NULL:
        	      default:
        	          break;
        	      
        	  };
        	  if($field != ""){
        	      ?><dt><?=$field?></dt><dd><input type="number" name="wmedia"></dd><?php     
        	  }
        	  ?><dt>&nbsp;</dt><dd><input type="submit" name="Crear"></dd>
        	</dl>
        	<input type="hidden" name="wtype" value="<?=$ty?>"/>
        </form>
        </div>
      </div>
      <footer>
        <p>That's all folks!</p>
      </footer>
    </div>
  </body>
</html>