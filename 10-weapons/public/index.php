<?php

require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/WeaponController.php');
require_once(__DIR__.'/../lib/model/Weapon.php');
require_once(__DIR__.'/../lib/model/WeaponImg.php');
require_once(__DIR__.'/../lib/model/WeaponYT.php');
require_once(__DIR__.'/../lib/model/WeaponSC.php');

//AQUI LLAMARIAMOS A LA SESSION

$cnt = new WeaponController();
$ws = $cnt->listAction();

?><html>
  <head>
    <title>Weapons' list</title>
    <link href="/css/styles.css" type="text/css" rel="stylesheet"/>
  </head>
  <body>
    <div id="wrapper">
      <header>
        <ul>
          <li>Home</li>
          <li>Other pages...</li>
        </ul>
      </header>
      <div id="content">
        <h1>Weapons' list</h1>
        <div id="submenu">
        	<ul>
        		<li><a href="/add.php?type=<?=W_TYPE_NULL?>">Añadir arma</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_IMG?>">Añadir arma imagen</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_YT?>">Añadir arma YT</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_SC?>">Añadir arma SC</a></li>
        	</ul>
        </div>
        <div id="w-container">
          <?php foreach($ws as $w){  include($w->getView()); }?>
          <p class="clear">&nbsp;</p>
        </div>
      </div>
      <footer>
        <p>That's all folks!</p>
      </footer>
    </div>
  </body>
</html>