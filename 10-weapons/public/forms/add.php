<?php

require_once(__DIR__.'/../../lib/inc/constants.php');
require_once(__DIR__.'/../../lib/controller/WeaponController.php');
require_once(__DIR__.'/../../lib/model/Weapon.php');
require_once(__DIR__.'/../../lib/model/WeaponImg.php');
require_once(__DIR__.'/../../lib/model/WeaponYT.php');
require_once(__DIR__.'/../../lib/model/WeaponSC.php');

//AQUI LLAMARIAMOS A LA SESSION

$cnt = new WeaponController();
$tipo = $_POST['wtype'];
$name = $_POST['wname'];
$origin = $_POST['worig'];
$material = $_POST['wmat']; 
$peso = $_POST['wpes'];
$filo = $_POST['wfil'];
$media = isset($_POST['wmedia']) ? $_POST['wmedia'] : "";

$wp = $cnt->createAction($tipo, $name, $material, $origin, $peso, $filo, $media);

?><html>
  <head>
    <title>Add Weapon</title>
    <link href="/css/styles.css" type="text/css" rel="stylesheet"/>
  </head>
  <body>
    <div id="wrapper">
      <header>
        <ul>
          <li>Home</li>
          <li>Other pages...</li>
        </ul>
      </header>
      <div id="content">
        <h1>Add Weapon</h1>
        <div id="submenu">
        	<ul>
        		<li><a href="/add.php?type=<?=W_TYPE_NULL?>">Añadir arma</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_IMG?>">Añadir arma imagen</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_YT?>">Añadir arma YT</a></li>
        		<li><a href="/add.php?type=<?=W_TYPE_SC?>">Añadir arma SC</a></li>
        	</ul>
        </div>
        <div id="w-container">
        	<dl>
        	  <dt>Nombre</dt><dd><?=$wp->getNombre()?></dd>
        	  <dt>Origen</dt><dd><?=$wp->getOrigen()?></dd>
        	  <dt>Material</dt><dd><?=$wp->getMaterial()?></dd>
        	  <dt>Filo</dt><dd><?=$wp->getFilo()?></dd>
        	  <dt>Peso</dt><dd><?=$wp->getPeso()?></dd>
        	  <?php
        	  $field ="";
        	  $value ="";
        	  switch ($wp->getType()){
        	      case W_TYPE_IMG:
        	          $field ="Imagen";
        	          $value =$wp->getImagen();
        	          break;
        	      case W_TYPE_YT:
        	          $field ="ID YouTube";
        	          $value =$wp->getYT();
        	          break;
        	      case W_TYPE_SC:
        	          $field ="ID SoundCloud";
        	          $value =$wp->getScid();
        	          break;
        	      case W_TYPE_NULL:
        	      default:
        	          break;
        	      
        	  };
        	  if($field != ""){
        	      ?><dt><?=$field?></dt><?=$value?></dd><?php     
        	  }
        	  ?>
        	</dl>
        </div>
      </div>
      <footer>
        <p>That's all folks!</p>
      </footer>
    </div>
  </body>
</html>