<div class="w-item">
  <h2><?=$w->getNombre()?></h2>
  <p><?=$w->getOrigen()?></p>
  <dl>
  <dt>Filo</dt><dd><?=$w->getFilo()?></dd>
  <dt>Peso</dt><dd><?=$w->getPeso()?>kg</dd>
  <dt>Material</dt><dd><?=$w->getMaterial()?></dd>
  </dl>
  <iframe width="300" height="169" src="https://www.youtube.com/embed/<?=$w->getYT()?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>