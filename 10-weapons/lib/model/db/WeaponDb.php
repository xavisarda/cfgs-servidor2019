<?php 

require_once(__DIR__.'/../Weapon.php');
require_once(__DIR__.'/../WeaponImg.php');
require_once(__DIR__.'/../WeaponSC.php');
require_once(__DIR__.'/../WeaponYT.php');

class WeaponDb{
    
    private $_conn;
    
    public function insertVoid($n, $m, $o, $p, $f){
        $fields = ["wtype", "nombre", "origen", "material", "filo", "peso", "vista"];
        $qs = ["?", "?", "?", "?", "?", "?", "?"];
        $values = [W_TYPE_NULL, $n, $o, $m, $f, $p, __DIR__.'/../../inc/weapon.php'];
        $types = "issssss";
        
        return $this->insert($fields, $qs, $values, $types);
    }
    
    public function insertImg($n, $m, $o, $p, $f, $img){
        $fields = ["wtype", "nombre", "origen", "material", 
            "filo", "peso", "vista", "media1"];
        $qs = ["?", "?", "?", "?", "?", "?", "?", "?"];
        $values = [W_TYPE_IMG, $n, $o, $m, $f, $p, 
            __DIR__.'/../../inc/weapon.php', $img];
        $types = "isssssss";
        
        return $this->insert($fields, $qs, $values, $types);
    }
    
    public function insertYT($n, $m, $o, $p, $f, $yt){
        $fields = ["wtype", "nombre", "origen", "material",
            "filo", "peso", "vista", "media2"];
        $qs = ["?", "?", "?", "?", "?", "?", "?", "?"];
        $values = [W_TYPE_YT, $n, $o, $m, $f, $p,
            __DIR__.'/../../inc/weapon.php', $yt];
        $types = "isssssss";
        
        return $this->insert($fields, $qs, $values, $types);
    }
    
    public function insertSC($n, $m, $o, $p, $f, $sc){
        $fields = ["wtype", "nombre", "origen", "material",
            "filo", "peso", "vista", "media3"];
        $qs = ["?", "?", "?", "?", "?", "?", "?", "?"];
        $values = [W_TYPE_SC, $n, $o, $m, $f, $p,
            __DIR__.'/../../inc/weapon.php', $sc];
        $types = "isssssss";
        
        return $this->insert($fields, $qs, $values, $types);
    }
    
    private function insert($fields, $qs, $values, $types){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();
        $fstr = implode(",", $fields);
        $qstr = implode(",", $qs);
        
        //PREPARE STATEMENT WITH SEVERAL PARAMETERS
        $query = "INSERT INTO ".DDBB_TABLE." (".$fstr.") VALUES (".$qstr.")";
        $stmt = $this->_conn->prepare($query);
        if (count($values) == 7) {
            $val = array(7);
            $stmt->bind_param($types, $values[0], $values[1], 
                $values[2], $values[3], $values[4], 
                $values[5], $values[6]);
        } else if(count($values) == 8) {
            $val = array(8);
            $stmt->bind_param($types, $values[0], $values[1], 
                $values[2], $values[3], $values[4], $values[5], 
                $values[6], $values[7]);
        }
        
        //EXECUTE QUERY
        $stmt->execute();
        
        //RETRIEVE NEW BOLET AND RETURN
        return $this->details($stmt->insert_id);
    }
    
    public function update(){
        
    }
    
    public function delete(){
        
    }
    
    public function list(){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();
        
        //PREPARE STATEMENT WITH NO PARAMETERS AT THE MOMENT
        $query = "SELECT * FROM ".DDBB_TABLE;
        $stmt = $this->_conn->prepare($query);
        
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
        
        //RETRIEVE RESULTS AND BUILD RETURN ARRAY
        $wpns = array();
        while ($wpn = $res->fetch_assoc() ) {
            array_push($wpns, $this->createWpnObject($wpn));
        }
        return $wpns;
    }
    
    public function details($idarma){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();
        
        //PREPARE STATEMENT WITH NO PARAMETERS AT THE MOMENT
        $query = "SELECT * FROM ".DDBB_TABLE." WHERE wid = ?";
        $stmt = $this->_conn->prepare($query);
        $stmt->bind_param("i", $index);
        $index = $idarma;
        
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
        
        //RETRIEVE RESULTS AND BUILD RETURN ARRAY
        $wpn = $res->fetch_assoc();
        return $this->createWpnObject($wpn);
    }
    
    private function createWpnObject($arma){
        $armaO = null;
        switch($arma['wtype']){
            case W_TYPE_NULL:
                $armaO = new Weapon($arma['nombre'], $arma['filo'], 
                $arma['origen'], $arma['material'], $arma['peso']);
                break;
            case W_TYPE_IMG:
                $armaO = new WeaponImg($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media1']);
                break;
            case W_TYPE_YT:
                $armaO = new WeaponYT($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media2']);
                break;
            case W_TYPE_SC:
                $armaO = new WeaponSC($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media3']);
                break;
        };
        return $armaO;
    }
    
    private function openConnection(){
        if($this->_conn == NULL){
            $this->_conn = mysqli_connect(DDBB_HOST, DDBB_USER, DDBB_PWD, DDBB_DDBB);
        }
    }
}