<?php 

require_once(__DIR__.'/Weapon.php');

class WeaponSC extends Weapon{
    
    private $_scid;
    
    public function __construct($n, $f, $o, $m, $p, $sc){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
        $this->setScid($sc);
    }
    
    public function getType(){
        return W_TYPE_SC;
    }
    
    public function getScid(){
        return $this->_scid;
    }

    public function setScid($_scid){
        $this->_scid = $_scid;
    }

    public function getView(){
        return __DIR__.'/../inc/weaponSC.php';
    }
    
}