<?php 

class Weapon{
    
    private $_nombre;
    private $_filo;
    private $_origen;
    private $_material;
    private $_peso;
    
    public function __construct($n, $f, $o, $m, $p){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
    }
    
    public function getType(){
        return W_TYPE_NULL;
    }
    
    public function getNombre(){
        return $this->_nombre;
    }

    public function getFilo(){
        return $this->_filo;
    }

    public function getOrigen(){
        return $this->_origen;
    }

    public function getMaterial(){
        return $this->_material;
    }

    public function getPeso(){
        return $this->_peso;
    }

    public function setNombre($_nombre){
        $this->_nombre = $_nombre;
    }

    public function setFilo($_filo){
        $this->_filo = $_filo;
    }

    public function setOrigen($_origen){
        $this->_origen = $_origen;
    }

    public function setMaterial($_material){
        $this->_material = $_material;
    }

    public function setPeso($_peso){
        $this->_peso = $_peso;
    }
    
    public function getView(){
        return __DIR__.'/../inc/weapon.php';
    }
    
}