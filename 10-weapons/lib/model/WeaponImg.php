<?php 

require_once(__DIR__.'/Weapon.php');

class WeaponImg extends Weapon{
    
    private $_imagen;
    
    public function __construct($n, $f, $o, $m, $p, $i){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
        $this->setImagen($i);
    }
    
    public function getType(){
        return W_TYPE_IMG;
    }
    
    public function getImagen(){
        return $this->_imagen;
    }

    public function setImagen($_imagen){
        $this->_imagen = $_imagen;
    }

    public function getView(){
        return __DIR__.'/../inc/weaponImg.php';
    }
    
}