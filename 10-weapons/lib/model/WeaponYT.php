<?php 

require_once(__DIR__.'/Weapon.php');

class WeaponYT extends Weapon{
    
    private $_yt;
    
    public function __construct($n, $f, $o, $m, $p, $yt){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
        $this->setYT($yt);
    }
    
    public function getType(){
        return W_TYPE_YT;
    }
    
    public function getYT(){
        return $this->_yt;
    }

    public function setYT($_yt){
        $this->_yt = $_yt;
    }

    public function getView(){
        return __DIR__.'/../inc/weaponYT.php';
    }
    
}