<?php

require_once(__DIR__.'/../model/db/WeaponDb.php');

class WeaponController{
    
    public function listAction(){
        $dbs = new WeaponDb();
        return $dbs->list();
    }
    
    public function detailsAction(){
        
    }
    
    public function deleteAction(){
        
    }
    
    public function updateAction(){
        
    }
    
    public function createAction($t, $n, $m, $o, $p, $f, $med){
        $objret = null;
        $dbs = new WeaponDb();
        switch($t){
            case W_TYPE_NULL:
                $objret = $dbs->insertVoid($n, $m, $o, $p, $f);
                break;
            case W_TYPE_IMG:
                $objret = $dbs->insertImg($n, $m, $o, $p, $f, $med);
                break;
            case W_TYPE_YT:
                $objret = $dbs->insertYT($n, $m, $o, $p, $f, $med);
                break;
            case W_TYPE_SC:
                $objret = $dbs->insertSC($n, $m, $o, $p, $f, $med);
                break;
            default:
                return null;
        };
        return $objret;
    }
    
}
