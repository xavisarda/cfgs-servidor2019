<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once(__DIR__.'/../ext/vendor/autoload.php');
require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/WeaponController.php');


$app = new \Slim\App;

$app->get('/weapons',function(
        Request $request, Response $response, array $args) {
    $cnt = new WeaponController();
    $wps = $cnt->listAction();

    $arrayW = array();
    foreach ($wps as $wp) {
      $arrayW[] = $wp->toArray();
    }

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($arrayW));
    return $newR;
});

$app->get('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $cnt = new WeaponController();

  $wobj = $cnt->detailsAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

$app->post('/weapons',function(
        Request $request, Response $response, array $args) {
    $wjson = $request->getParsedBody();
    $cnt = new WeaponController();

    $newwp = $cnt->createAction($wjson['tipo'], $wjson['nombre'],
        $wjson['material'], $wjson['origen'], $wjson['peso'],
        $wjson['filo'], $wjson['media']);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($newwp->toArray()));
    return $newR;
});

$app->delete('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $cnt = new WeaponController();

  $wobj = $cnt->deleteAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

$app->put('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $wjson = $request->getParsedBody();
  $cnt = new WeaponController();

  $newwp = $cnt->createAction($wjson['tipo'], $wjson['nombre'],
      $wjson['material'], $wjson['origen'], $wjson['peso'],
      $wjson['filo'], $wjson['media']);

  $wobj = $cnt->deleteAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

$app->run();
