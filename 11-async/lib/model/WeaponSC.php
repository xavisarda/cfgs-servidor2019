<?php 

require_once(__DIR__.'/Weapon.php');

class WeaponSC extends Weapon{
    
    private $_scid;
    
    public function __construct($n, $f, $o, $m, $p, $sc, $wid = null){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
        $this->setScid($sc);
        $this->setWid($wid);
    }
    
    public function getType(){
        return W_TYPE_SC;
    }
    
    public function getScid(){
        return $this->_scid;
    }

    public function setScid($_scid){
        $this->_scid = $_scid;
    }

    public function getView(){
        return __DIR__.'/../inc/weaponSC.php';
    }
    
    public function toArray(){
        $obj = array();
        $obj['nombre'] = $this->getNombre();
        $obj['filo'] = $this->getFilo();
        $obj['origen'] = $this->getOrigen();
        $obj['material'] = $this->getMaterial();
        $obj['peso'] = $this->getPeso();
        $obj['media3'] = $this->getScid();
        $obj['tipo'] = W_TYPE_SC;
        if($this->getWid() != NULL){
            $obj['wid'] = $this->getWid();
        }
        
        return $obj;
    }
}