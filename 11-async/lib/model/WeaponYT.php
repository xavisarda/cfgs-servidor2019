<?php 

require_once(__DIR__.'/Weapon.php');

class WeaponYT extends Weapon{
    
    private $_yt;
    
    public function __construct($n, $f, $o, $m, $p, $yt, $wid = null){
        $this->setNombre($n);
        $this->setFilo($f);
        $this->setOrigen($o);
        $this->setMaterial($m);
        $this->setPeso($p);
        $this->setYT($yt);
        $this->setWid($wid);
    }
    
    public function getType(){
        return W_TYPE_YT;
    }
    
    public function getYT(){
        return $this->_yt;
    }

    public function setYT($_yt){
        $this->_yt = $_yt;
    }

    public function getView(){
        return __DIR__.'/../inc/weaponYT.php';
    }
    
    public function toArray(){
        $obj = array();
        $obj['nombre'] = $this->getNombre();
        $obj['filo'] = $this->getFilo();
        $obj['origen'] = $this->getOrigen();
        $obj['material'] = $this->getMaterial();
        $obj['peso'] = $this->getPeso();
        $obj['media2'] = $this->getYT();
        $obj['tipo'] = W_TYPE_YT;
        if($this->getWid() != NULL){
            $obj['wid'] = $this->getWid();
        }
        
        return $obj;
    }
    
}