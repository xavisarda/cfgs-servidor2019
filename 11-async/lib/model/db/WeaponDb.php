<?php


require_once(__DIR__.'/../Weapon.php');
require_once(__DIR__.'/../WeaponImg.php');
require_once(__DIR__.'/../WeaponSC.php');
require_once(__DIR__.'/../WeaponYT.php');

class WeaponDb{

    private $_conn;

    public function insertVoid($n, $m, $o, $p, $f){
        $obj = new Weapon($n, $f, $o, $m, $p);
        return $this->insert($obj);
    }

    public function insertImg($n, $m, $o, $p, $f, $img){
        $obj = new WeaponImg($n, $f, $o, $m, $p, $img);
        return $this->insert($obj);
    }

    public function insertYT($n, $m, $o, $p, $f, $yt){
        $obj = new WeaponYT($n, $f, $o, $m, $p, $yt);
        return $this->insert($obj);
    }

    public function insertSC($n, $m, $o, $p, $f, $sc){
        $obj = new WeaponSC($n, $f, $o, $m, $p, $sc);
        return $this->insert($obj);
    }

    private function insert($wObject){
        $collection = $this->getCollection();
        $insertOneResult = $collection->insertOne($wObject->toArray());

        $idIns = $insertOneResult->getInsertedId()->__toString();
        return $this->details($idIns);
    }

    public function update($idarma, $t, $n, $m, $o, $p, $f, $med){
      $collection = $this->getCollection();
      $setupdate = array();
      $setupdate['nombre'] = $n;
      $setupdate['tipo'] = $t;
      $setupdate['material'] = $m;
      $setupdate['origen'] = $o;
      $setupdate['peso'] = $p;
      $setupdate['filo'] = $f;

      switch ($t) {
        case W_TYPE_NULL:
            break;
        case W_TYPE_IMG:
            $setupdate['media1'] = $med;
            break;
        case W_TYPE_YT:
            $setupdate['media2'] = $med;
            break;
        case W_TYPE_SC:
            $setupdate['media3'] = $med;
            break;
      }

      $insertOneResult = $collection->updateOne(
        ["_id" => new MongoDB\BSON\ObjectId($idarma)],
        [ '$set' => $setupdate ]);

      return $this->details($idarma);
    }

    public function delete($idarma){
      $collection = $this->getCollection();
      $w = $collection->findOne(
          ["_id" => new MongoDB\BSON\ObjectId($idarma)]);

      $wo = $this->createWpnObject($w);

      $collection->deleteOne(["_id" => new MongoDB\BSON\ObjectId($idarma)]);

      return $wo;
    }

    public function list(){
        $collection = $this->getCollection();
        $list = $collection->find();

        $wlist = array();
        foreach ($list as $wpmdb){
            $wpn = $this->createWpnObject($wpmdb);
            array_push($wlist,$wpn);
        }
        return $wlist;
    }

    public function details($idarma){
        $collection = $this->getCollection();
        $w = $collection->findOne(
            ["_id" => new MongoDB\BSON\ObjectId($idarma)]);

        $wo = $this->createWpnObject($w);

        return $wo;
    }

    private function createWpnObject($arma){
        $armaO = null;
        switch($arma['tipo']){
            case W_TYPE_NULL:
                $armaO = new Weapon($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso'],
                $arma['_id']->__toString());
                break;
            case W_TYPE_IMG:
                $armaO = new WeaponImg($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media1'], $arma['_id']->__toString());
                break;
            case W_TYPE_YT:
                $armaO = new WeaponYT($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media2'], $arma['_id']->__toString());
                break;
            case W_TYPE_SC:
                $armaO = new WeaponSC($arma['nombre'], $arma['filo'],
                $arma['origen'], $arma['material'], $arma['peso']
                , $arma['media3'], $arma['_id']->__toString());
                break;
        };
        return $armaO;
    }

    private function getCollection(){
       return (new MongoDB\Client)->wdb->wpns;
    }

}
