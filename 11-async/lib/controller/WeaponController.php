<?php

require_once(__DIR__.'/../model/db/WeaponDb.php');

class WeaponController{

    public function listAction(){
        $dbs = new WeaponDb();
        return $dbs->list();
    }

    public function detailsAction($idw){
        $dbs = new WeaponDb();
        return $dbs->details($idw);
    }

    public function deleteAction($idw){
      $dbs = new WeaponDb();
      return $dbs->delete($idw);
    }

    public function updateAction($idw, $t, $n, $m, $o, $p, $f, $med){
      $dbs = new WeaponDb();
      return $dbs->udpate($idw, $t, $n, $m, $o, $p, $f, $med);
    }

    public function createAction($t, $n, $m, $o, $p, $f, $med){
        $objret = null;
        $dbs = new WeaponDb();
        switch($t){
            case W_TYPE_NULL:
                $objret = $dbs->insertVoid($n, $m, $o, $p, $f);
                break;
            case W_TYPE_IMG:
                $objret = $dbs->insertImg($n, $m, $o, $p, $f, $med);
                break;
            case W_TYPE_YT:
                $objret = $dbs->insertYT($n, $m, $o, $p, $f, $med);
                break;
            case W_TYPE_SC:
                $objret = $dbs->insertSC($n, $m, $o, $p, $f, $med);
                break;
            default:
                return null;
        };
        return $objret;
    }

}
