<?php

require_once(__DIR__.'/lib/controller/ProductsController.php');
require_once(__DIR__.'/lib/model/Product.php');

$cnt = new ProductsController();
$prods = $cnt->getProductList();
$i = 0;
?><html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <div id="wrapper">
    <?php require_once(__DIR__.'/inc/header.php'); ?>
      <h1>Product List</h1>
      <ul>
    	  <?php foreach($prods as $p){ ?>
    	  <li><a href="/details.php?prod=<?=$i?>"><?=$p->getName()?>:<?=$p->getDesc()?> <?=$p->getPrice()?>(€)</a></li>
     	  <?php $i++; } ?>
      </ul>
      <img src="/img/seta.jpg"/>
    </div>
  </body>
</html>
