<?php

require_once(__DIR__.'/lib/controller/ProductsController.php');
require_once(__DIR__.'/lib/model/Product.php');

$index = $_GET['prod'];

$cnt = new ProductsController();
$prod = $cnt->getProductByIndex($index);

?><html>
  <head>
    <title>Product details</title>
  </head>
  <body>
    <div id="wrapper">
    <?php require_once(__DIR__.'/inc/header.php'); ?>
      <h1>Product details</h1>
      <h2><?=$prod->getName()?></h2>
      <p><?=$prod->getDesc()?></p>
      <span>Now only!!! <?=$prod->getPrice()?></span>
    </div>
  </body>
</html>
