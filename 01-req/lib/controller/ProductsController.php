<?php 

require_once(__DIR__.'/../model/Product.php');

class ProductsController{
    
    public function getProductList(){
        $prods = array();
        
        array_push($prods, new Product("Foo Product", "Foo desc", 10));
        array_push($prods, new Product("Foo Product", "Foo desc", 20));
        array_push($prods, new Product("Foo Product", "Foo desc", 30));
        array_push($prods, new Product("Foo Product", "Foo desc", 40));
        array_push($prods, new Product("Foo Product", "Foo desc", 50));
        
        return $prods;
    }
    
    public function getProductByIndex($index){
        $prodlist = $this->getProductList();
        return $prodlist[$index];
    }
    
}