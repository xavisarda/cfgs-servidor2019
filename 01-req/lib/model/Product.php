<?php 


class Product{
    
    private $_price;
    private $_name;
    private $_desc;
    
    public function __construct($n = "", $d = "", $p = 0) {
        $this->setName($n);
        $this->setDesc($d);
        $this->setPrice($p);
    }
    
    public function getPrice() {
        return $this->_price;
    }

    public function getName() {
        return $this->_name;
    }

    public function getDesc() {
        return $this->_desc;
    }

    public function setPrice($_price) {
        $this->_price = $_price;
    }

    public function setName($_name) {
        $this->_name = $_name;
    }

    public function setDesc($_desc) {
        $this->_desc = $_desc;
    }

}
