
function addCar(){
	var car = { "brand" : $("#brand").val(),
				"model" : $("#model").val(),
				"year"	: $("#year").val() };

	$.ajax({
		url:"/cars",
		method:"post",
		accept:"json",
		data : car,
		dataType: "json"
	}).done(function(response){
		$("#brand").val('');
		$("#model").val('');
		$("#year").val('');
		getAllCars();
	});

}

function detailsCar(carid){
	$.ajax({
		url:"/cars/"+carid,
		method: "get",
		accept: "text/json"
	}).done(function(response){
		var htmlcars = '';		
			htmlcars += '<div>';
			htmlcars += '<h3><span>'+response.brand+'-'+response.model+'</span></h3>';
			htmlcars += '<p>Year: '+response.year+'</p>';
			htmlcars += '<p><span onclick="detailsCar(\''+response.cid+'\')">View</span></br>';
			htmlcars += '<span onclick="deleteCar(\''+response.cid+'\')">Delete</span></p>';
			htmlcars += '</div>';
		$('#detall').html(htmlcars);
	});
}

function deleteCar(carid){
	$.ajax({
		url:"/cars/"+carid,
		method:"delete",
		accept:"json"
	}).done(function(response){
		getAllCars();
	});
}

function getAllCars(){
	$.ajax({
		url:"/cars",
		method: "get",
		accept: "text/json"
	}).done(function(response){
		var htmlcars = '';
		$.each(response, function(index,value){
			htmlcars += '<div>';
			htmlcars += '<h3><span>'+value.brand+'-'+value.model+'</span></h3>';
			htmlcars += '<p>Year: '+value.year+'</p>';
			htmlcars += '<p><span onclick="detailsCar(\''+value.cid+'\')">View</span></br>';
			htmlcars += '<span onclick="deleteCar(\''+value.cid+'\')">Delete</span></p>';
			htmlcars += '</div>';
		});
		$('#llistat').html(htmlcars);
	});

}
