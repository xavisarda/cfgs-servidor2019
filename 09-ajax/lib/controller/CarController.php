<?php 

require_once(__DIR__.'/../model/Car.php');
require_once(__DIR__.'/../model/db/CarDb.php');

class CarController{
    
    public function addCar($b, $m, $y){
        $car = new Car($b, $m, $y);
        $cardb = new CarDb();
        
        $id = $cardb->insertCar($car);
        return $id;
    }
    
    public function listCars(){
        $cardb = new CarDb();
        return $cardb->listCars();
    }
    
    public function deleteCarbyId($cid){
        $cardb = new CarDb();
        return $cardb->removeCarbyId($cid);
    }
    
    public function detailsCar($cid){
        $cardb = new CarDb();
        return $cardb->getCarbyId($cid);
    }
    
}