<?php 

class Car{
    
    private $_brand;
    private $_model;
    private $_year;
    private $_cid;
    
    public function __construct($brand="", $model="", $year=2000, $carid = null){
        $this->setBrand($brand);
        $this->setModel($model);
        $this->setYear($year);
        $this->setCid($carid);
    }
    
    public function getBrand()
    {
        return $this->_brand;
    }

    public function getModel()
    {
        return $this->_model;
    }

    public function getYear()
    {
        return $this->_year;
    }

    public function getCid()
    {
        return $this->_cid;
    }

    public function setCid($_cid)
    {
        $this->_cid = $_cid;
    }

    public function setBrand($_brand)
    {
        $this->_brand = $_brand;
    }

    public function setModel($_model)
    {
        $this->_model = $_model;
    }

    public function setYear($_year)
    {
        $this->_year = $_year;
    }
    
    public function toArray(){
        $ret = [ "brand" => $this->getBrand(),
                "model" => $this->getModel(),
                "year" => $this->getYear()
        ];
        if($this->getCid() != null){
            $ret['cid'] = $this->getCid();
        }
        return $ret;
    }
    
    public function toJSON(){
        return json_encode($this->toArray());
    }
    
}