<?php 

require_once(__DIR__.'/../Car.php');

class CarDb{
    
    public function insertCar($car){
        $collection = (new MongoDB\Client)->cardb->car;
        $insertOneResult = $collection->insertOne($car->toArray());
        
        return $insertOneResult->getInsertedId()->__toString();
    }
    
    public function listCars(){
        $collection = (new MongoDB\Client)->cardb->car;
        $list = $collection->find();
        
        $carlist = array();
        foreach ($list as $carl){
            array_push($carlist,
                new Car($carl['brand'], $carl['model'], 
                    $carl['year'], $carl['_id']->__toString()));
        }
        return $carlist;
    }
    
    public function removeCarbyId($id){
        $car = $this->getCarbyId($id);
        $collection = (new MongoDB\Client)->cardb->car;
        $carl = $collection->deleteOne(["_id" => new MongoDB\BSON\ObjectId($id)]);
        
        return $car;
    }
        
    public function getCarbyId($id){
        $collection = (new MongoDB\Client)->cardb->car;
        $carl = $collection->findOne(["_id" => new MongoDB\BSON\ObjectId($id)]);
        $c = new Car($carl['brand'], $carl['model'],
            $carl['year'], $carl['_id']->__toString());
        
        return $c;
    }
    
}
